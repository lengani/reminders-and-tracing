Follow ups for Exposed Children
===============================

All babies should be registered by HCWs as soon as they are born. The
registration process links them to their mother. A particular HSA can
also be linked for follow ups for a particular child as outlined
below.

Registration of babies
----------------------

The MWANA keyword is used to register babies using the following format:

    **MWANA <DATE_OF_BIRTH> <MOTHERS_ID> <CHILDS_BIRTHPOSITION> <OPTIONAL:HSA_PHONE>**

Where:

DATE_OF_BIRTH
  The date of birth of the baby in YYYY-MM-DD format.

MOTHERS_ID
  The unique identifier for mother.

CHILDS_BIRTHPOSITION
  This is the ordinal position of birth of the child. They can add the 
  suffix "BORN" at the end.

HSA_PHONE
  The phone number for the HSA who is supposed to follow up or receive reminders
  for this particular child. If this is absent the HSA_PHONE assigned will
  be that of the sender.

For example:

    **MWANA 270814 250109345 SECONDBORN**

To link the follow up of a baby to a specific HSA, the sender can append
the phone number of the HSA to the message.

The second born child will now be linked to the mother with
ID 250109345. If the mother was registered as exposed the reminders
for exposed clients will also be sent to the HCW who registered the
mother or to the mother if she opted to receive reminders to ensure
that the mother takes the child to for the EID appointments when they are
due.

In this case the ID for the child will be SECONDBORN250109345.

In addition to all the reminders for care for babies,
mothers/guardians of exposed infants will also receive the following
reminders if they opted in. Alternatively, these reminders will be
sent to CHWs/Health SA for mothers/guardians who did not provide
mobile numbers. These agents will then deliver messages to the
mother/guardians:

#. Reminder for DBS collection at 6 Weeks.
#. Reminder for confirmatory Rapid Test at 12 months.
#. Reminder for confirmatory DNA/PCR test at 24 months.

The reminders are all sent a week before the actual date of the appointment.

Results collection notices
--------------------------

The system will also automatically send out notifications that DNA_PCR
results are ready for collection as soon as it receives confirmations
on receipt of results at the facility via SMS printer, mobile phone or
hard copy.

When the DNA_PCR test results are sent to the facility, the health
care worker should initiate a notification to mothers who provided
phone numbers, or CHW/Health SA for the follow up that
the results are ready for collection. This is done using the COLLECT
keyword as follows:

    **COLLECT <SAMPLE_ID>**

This will trigger the system to send a notification to the number that
was recorded on a requisition form during dbs collection that the test
result is ready for collection.

If a notification was already sent the system will respond with
details specifying the number to which the notification was sent. If a number
was not registered for notification on sample collection, the system
will report the same to the health worker requesting the collection of
the results.
