Reporting
=========

Reports are available on the dashboard from the data generated from
the registration of mothers and children, the appointments that are
generated and the tracing of mothers and children.

These are all available from the "RemindMI" link in the main
navigation as shown below:

.. image:: /_static/navigation.png

These reports have the following features, most of which are common
to all.

Report features
---------------

The following are some of the filters and functions that are available
on the reports.

Date range selection
   You can select a date range for a specified column. For example, a
   date for when mothers were registered. Date selection has some
   preset date ranges but custom date ranges can also be selected
   using the custom range button.

.. image:: /_static/date-range.png


HMIS
  You can specify the four digit HMIS facility code to filter the
  reports by facility.


Name
  You can filter the report specifying the unique id or name used to
  register the client.


Districts
  You can filter the report by selecting one or more districts for the
  data that is presented.


Table sorting
  You can sort the ordering of the reports by clicking the column headers.


Data exports
  You can download the report as a CSV or Excel file by clicking the
  relevant icon in the bottom right corner of the page.



.. image:: /_static/exports.png


Mothers
-------

.. image:: /_static/mothers.png

This report shows all mothers registered by facility and date. This
has the following filters to allow the selection of mothers as
follows:

- Registration date range
- HMIS (Facility code)
- Name
- Districts

Children
--------

.. image:: /_static/children.png

This report shows all registered babies by facility and date.


Appointments
------------

.. image:: /_static/appointments.png

This shows all missed appointments by facility and date.


Traces
------

.. image:: /_static/traces.png

This report allows viewing all traced patients by facility and date, and by use
of the available filters:

 - All patients traced who did not return by facility and date.
 - All patients traced and returned in the continuum of care by facility and date.
 - All patients traced and died by facility and date.
 - All patients traced and not found by facility and date.

Reminders
---------

.. image:: /_static/reminders.png

This report shows all appointment reminders that were sent directly to
mothers by facility and date.


EID
---

.. image:: /_static/eid.png

This report shows all babies with positive results initiated on
treatment by facility and date. The filters also allow showing all
babies confirmed HIV-negative at 24 months and removed from the system
by facility and date.
