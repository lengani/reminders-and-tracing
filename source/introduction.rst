Introduction
============

The following are the requirements for patient follow up:

#. Follow and trace pregnant HIV+ mothers for antenatal visits and ART refill visits.

   a. Tracing all antenatal visits.
   b. Tracing ART refills.

#. Follow, trace and return babies in the continuum of care

   a. Record the date when the baby is born.
   b. DBS collections at six weeks.
   c. DNA-PCR Results collections.
   d. Rapid test at 12 months.
   e. Confirmatory test at 24 months.
   f. Initiation on treatment.

Conventions
-----------

The following conventions are used in this manual.

**Outgoing Message from phone number 0987654321:**

    **0987654321>>** HELP

**Incoming Message to phone number 0987654321:**

    **0987654321<<** Sorry you are having trouble ...
