Follow ups for Women
====================

All women found to be pregnant must be registered in RapidSMS by HCWs
at the facility. The registration links the mother with the CHW/HSA,
and Health Facility where the mother reports and the phone number
where the reminder SMS will be sent should the mother default on an
appointment.

The registration also allows mothers to be linked to a particular HSA
as outlined below.


Registering a mother
--------------------

All expectant mothers should be registered for care to ensure that they receive
reminders for necessary appointments.

The MAYI keyword is used to register mothers with the following format:

    **MAYI <EDD> <MOTHERS_ID> <DOB> <MOTHERS_PHONE> <HIV_STATUS> <OPTIONAL:HSA_PHONE>**

Where:

EDD
  The expected delivery date for the mother.

MOTHERS_ID
  The mothers patient ID. This could be an ART number of the mother if on treatment.

DOB
  The date of birth for the mother.

MOTHERS_PHONE
  The mothers phone number in cases where she opts in. e.g. 0999888777

HIV_STATUS
  The status of the mother. Possible options are:
  P = Positive
  N = Negative
  U = Unknown

HSA_PHONE
  The phone number for the HSA who is supposed to follow up or receive reminders
  for this particular mother. If this is absent the HSA_PHONE assigned will
  be that of the sender.

If a value is not known for any of the fields, please enter a "X" for that field.

For example:

    **MAYI 270814 250109345 120482 0987654321 P**


Tracing a mother
----------------

When a mother defaults on a scheduled appointment, the HCW at the
facility can initiate a trace for the mother using the TRACE keyword.

    **TRACE <MOTHERS_ID>**

For example:

    **TRACE 250109345**

If a mother was not registered with her phone number the trace request
sends out a request to the CHW/HSA who registered the mother in that facility
to find and remind the mother to attend her most recent appointment.

CHWs/Health SA must provide feedback every time they follow up
mothers. They can do this using the STATUS keyword which uses the
following format:

    **STATUS <TIMELINE> <MOTHERS_ID> <STATUS:FOUND|REFUSED|LOST|MOVED|DEAD>**

Where:

TIMELINE
  The timeline for the appointment for which the trace was requested.

MOTHERS_ID
  The mothers patient ID.

STATUS
  The update on the status after tracing the mother. The options are as follows:
  FOUND = "Found and returning"
  REFUSED = " Found but not returning"
  LOST = "Not found"
  MOVED = "Transferred"
  DEAD = "Deceased"

For example:

    **STATUS EXP 250109345 FOUND**

If the mother opted to receive reminders on her phone, she will
receive a reminder message for her to attend the most recent
appointment that was scheduled.

The mother can confirm that she has read the reminder and will attend
the appointment by responding using the CONFIRM keyword which uses the
format below:

    **CONFIRM <MOTHERS_ID>**

For example:

    **CONFIRM 250109345**

