============
Background
============

The following user manual details the use of the functionalities that have been added to RapidSMS to support the implementation of Mother-Infant Pair (MIP) clinics, a care delivery model tested for improved retention in the Prevention of Mother To Child Transmission (PMTCT) Program.

The system is used to remind mothers in the MIP clinics by providing effective follow ups of mothers who miss appointments. Follow up for mothers who have missed an appointment is done by Community Health Workers (CHWs) and Health Surveillance Assistance (HSAs).

The included patient follow up functionalities allow the CHWs and HSAs to receive follow-up instructions from the facility where the defaulter was first identified and also enhance the delivery of EID results to mothers/guardians following the delivery of the results to the facility.
