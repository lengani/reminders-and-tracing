Patient Follow Up User Manual
=============================

The following user manual describes the functionalities that have been
added to RapidSMS by the expansion of reminders, tracing and reporting
to support Mother-Infant-Pair clinics.

Contents:
---------

.. toctree::
   :maxdepth: 2

   background
   introduction
   women
   children
   initiation
   reporting



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
