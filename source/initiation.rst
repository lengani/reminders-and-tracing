Initiation of Babies on Treatment
---------------------------------

Following the delivery of DBS test results at the facility, HCWs should
send a message confirming initiation on treatment of all HIV positive
babies. The message should contain the HCC and new ART number as
specified in the format for the EID keyword as follows:

    **EID <SAMPLE_ID> <HIV> <ACTION_TAKEN: ART|CPT> <AGE_AT ART|CPT> <ART_NUMBER>**

Where:

SAMPLE_ID
  The sample id for the patient without spaces or punctuation.

HIV
  The HIV status. Options are:
  P = Positive
  N = Negative
  U = Unknown

ACTION_TAKEN
  The action that was taken after the result was delivered to the client. Options are:
  ART and CPT or X if no data is available.

AGE_AT_ART|CPT
  This is the age in months of the baby at ART/CPT.


For example:

    **EID 600191034 P ART 7 6001-HCC-1034**

ART Refills
-----------

As appointments for ART refills are usually variable, the HCW can schedule
appointments for refills for a registered client using the REFILL keyword as follows:

    **REFILL <MOTHERS_ID> <DATE:YYYY-MM-DD>**

This will create an appointment for the mother to collect ARTs on the specified date.

For example:

    **REFILL 600191034 2014-10-04**

This would create an appointment for the mother to collect ARTs on the 4th of October 2014.

